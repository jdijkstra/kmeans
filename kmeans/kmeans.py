# -*- coding: utf-8 -*-

import sys, random, math, copy, numpy as np
import matplotlib.pyplot as plt
from Toolbox.clusterPlot import clusterPlot

def calculate_mean(cluster):
    """calculates the mean of a cluster with labels"""
    leng = len(cluster)
    totals = [0, 0, 0 ,0]

    for i in range (leng):
        totals[0] += cluster[i][0][0]
        totals[1] += cluster[i][0][1]
        totals[2] += cluster[i][0][2]
        totals[3] += cluster[i][0][3]

    return [totals[0]/leng, totals[1]/leng, totals[2]/leng, totals[3]/leng]


def calculate_mean_of_list(mylist):
    """calculates the mean of a cluster without labels"""
    leng = len(mylist)
    totals = [0, 0, 0, 0]
    
    for i in range (leng):
        totals[0] += mylist[i][0]
        totals[1] += mylist[i][1]
        totals[2] += mylist[i][2]
        totals[3] += mylist[i][3]
    
    return [totals[0]/leng, totals[1]/leng, totals[2]/leng, totals[3]/leng]


def calculate_min_max(data):
    """calculates the boundaries of the data in the graph"""
    min_attr1 = sys.maxsize
    min_attr2 = sys.maxsize
    min_attr3 = sys.maxsize
    min_attr4 = sys.maxsize
    max_attr1 = -9999
    max_attr2 = -9999
    max_attr3 = -9999
    max_attr4 = -9999
    
    for i in range (len(data)):
        min_attr1 = min(min_attr1, data[i][0])
        min_attr2 = min(min_attr2, data[i][1])
        min_attr3 = min(min_attr3, data[i][2])
        min_attr4 = min(min_attr4, data[i][3])

        max_attr1 = max(max_attr1, data[i][0])
        max_attr2 = max(max_attr2, data[i][1])
        max_attr3 = max(max_attr3, data[i][2])
        max_attr4 = max(max_attr4, data[i][3])
       
    return [min_attr1, min_attr2, min_attr3, min_attr4], \
        [max_attr1, max_attr2, max_attr3, max_attr4]


def euclidean_distance(a, b):
    """Calculates the euclidean distance between datapoints"""
    dist = 0
    for i in range (len(a)):
        dist += pow(abs(a[i] - b[i]), 2)
    return math.sqrt(dist)


def pick_initial_centroids(min_array, max_array):
    """Pick random initial centroids by choosing random values between min and max of all attributes"""
    centroid_one = []
    centroid_two = []
    centroid_three = []
    for i in range (len(min_array)):
        centroid_one.append(random.randint(int(min_array[i]), int(max_array[i])))
        centroid_two.append(random.randint(int(min_array[i]), int(max_array[i])))
        centroid_three.append(random.randint(int(min_array[i]), int(max_array[i])))
    return centroid_one, centroid_two, centroid_three


def assign_objects(data, centroids):
    """Assign objects by seeing to which centroid it is closest to"""
    assigned_objects = []
    
    for data_object in range (len(data)):
            distances = []
            for centroid in range (len(centroids)):
                distances.append(euclidean_distance(data[data_object], centroids[centroid]))
            
            min_index = np.argmin(distances)
            cluster_no = min_index
            assigned_objects.append( (data[data_object], cluster_no) )
    return assigned_objects


def pop_class_labels(data):
    """removes the class labels from the data"""
    class_labels = []
    for i in range (len(data)):
        class_label = data[i].pop()
        class_labels.append(class_label)
    return data, class_labels


def form_clusters(assigned_objects):
    """forms clusters"""
    cluster_one = []
    cluster_two = []
    cluster_three = []
    for i in range (len(assigned_objects)):
        if assigned_objects[i][1] == 0:
            cluster_one.append(assigned_objects[i])
        elif assigned_objects[i][1] == 1:
            cluster_two.append(assigned_objects[i])
        else:
            cluster_three.append(assigned_objects[i])
    return cluster_one, cluster_two, cluster_three

    
def change_centroids(cluster_one, cluster_two, cluster_three):
    """Finds new centroids by calculating the mean of each cluster"""
    centroids = []
    centroids.append(calculate_mean(cluster_one))
    centroids.append(calculate_mean(cluster_two))
    centroids.append(calculate_mean(cluster_three))
    return centroids


def find_closest_pair_in_graph(irisdata):
    """finds closest pair of data objects in graph"""
    dummy = copy.deepcopy(irisdata)
    closest_pair = {}
    min_dist = sys.maxsize
    for i in range (len(irisdata)):
        pair, irisdata = find_closest_nabor(irisdata, irisdata[i])
        pair = pair[0]
        dist = pair[2]
        if dist < min_dist:
            min_dist = dist
            closest_pair[0] = pair
    return closest_pair[0], dummy


def find_closest_nabor(irisdata, data_obj):
    """finds closest nabor to a specified data object"""
    dummy = copy.deepcopy(irisdata)
    
    closest_pair = [0]
    min_dist = sys.maxsize
    if data_obj in irisdata:
        irisdata.pop(irisdata.index(data_obj))
    
    for i in range (len(irisdata)):
        if euclidean_distance(data_obj, irisdata[i]) < min_dist:
            min_dist = euclidean_distance(data_obj, irisdata[i])
            closest_pair[0] = (data_obj, irisdata[i], min_dist)
    return closest_pair, dummy 


def step_2_and_3(data, new_dataset):
    """step 2 and 3 of the suggested improvement to the k-means algorithm"""
    search, data = find_closest_pair_in_graph(data)
    closest_pair = (search[0], search[1])
    new_dataset.append(data.pop(data.index(closest_pair[0])))
    new_dataset.append(data.pop(data.index(closest_pair[1])))
    return data, new_dataset


def step_4_and_5(data, new_dataset):
    """step 4 and 5 of the suggested improvement to the k-means algorithm"""
    while(len(data) >= len(new_dataset)):    
        min_dist = sys.maxsize
        new_pair = {}
        for elem in range (len(new_dataset)):
            pair, data = find_closest_nabor(data, new_dataset[elem])
            pair = pair[0]
            dist = pair[2]
            if dist < min_dist:
                min_dist = dist
                new_pair[0] = pair
        new_entry = new_pair[0][1]
        data.pop(data.index(new_entry))
        new_dataset.append(new_entry)
    return data, new_dataset


def improved_k_means_algorithm(data, k):
    """the improved k-means algorithm"""
    dummy = copy.deepcopy(data)
    #step 1
    A_1 = []
    A_2 = []
    A_3 = []
    
    #step 2 and 3
    data, A_1 = step_2_and_3(data, A_1)

    #step 4 and 5
    data, new_dataset = step_4_and_5(data, A_1)

    #step 6:
    data, A_2 = step_2_and_3(data, A_2)
    data, A_2 = step_4_and_5(data, A_2)

    data, A_3 = step_2_and_3(data, A_3)
    data, A_3 = step_4_and_5(data, A_3) 

    centroids = []
    centroids.append(calculate_mean_of_list(A_1))
    centroids.append(calculate_mean_of_list(A_2))
    centroids.append(calculate_mean_of_list(A_3))
    
    #now we use the systematically found centroids for the standard k-means algorithm
    assigned_objects, final_centroids = standard_k_means_algorithm(dummy, centroids)
    return assigned_objects, final_centroids
    


def standard_k_means_algorithm(data, centroids):
    """The body of the standard k-means clustering algorithm"""
    assigned_objects = assign_objects(data, centroids)
    cluster_one, cluster_two, cluster_three = form_clusters(assigned_objects)
    new_centroids = change_centroids(cluster_one, cluster_two, cluster_three)
    if (new_centroids != centroids):
        return standard_k_means_algorithm(data, new_centroids)
    else:
        return assigned_objects, new_centroids
    
    
def main():
    
    k = 3
    
    irisdata = np.loadtxt("irisdata.txt")
    irisdata, class_labels = pop_class_labels(irisdata.tolist())
    dummy = copy.deepcopy(irisdata)
    
    labels = []
    for i in range (len(class_labels)):
        labels.append([int(class_labels[i])])

    min_array, max_array = calculate_min_max(irisdata)
    
    centroids = pick_initial_centroids(min_array, max_array)

    assigned_objects, final_centroids = standard_k_means_algorithm(irisdata, centroids)

    pred_labels = []
    for i in range (len(assigned_objects)):
        pred_labels.append(assigned_objects[i][1])
        
    clusterPlot(irisdata, pred_labels, final_centroids, labels)
    plt.title("standard k-means algorithm")
    plt.show()

    assigned_objects_improved, final_centroids_improved = improved_k_means_algorithm(irisdata, k)
    
    pred_labels_improved = []
    for i in range (len(assigned_objects_improved)):
        pred_labels_improved.append(assigned_objects_improved[i][1])

    clusterPlot(dummy, pred_labels_improved, final_centroids_improved, labels)
    plt.title("improved k-means algorithm")
    plt.show()
    

if __name__ == "__main__":
    sys.exit(main())